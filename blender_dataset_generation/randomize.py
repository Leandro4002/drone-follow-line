# Python script to be used inside Blender 3D modeling program
# It randomly moves, twists and/or make some objects disappear. It also moves around the white line.

from mathutils import Euler
import random
import math

for iteration in range(500):
    bpy.data.objects["camera"].location.y = random.uniform(-14, -10)
    bpy.data.objects["wall_east"].location.y = random.uniform(-7.5, -6)
    bpy.data.objects["wall_west"].location.y = random.uniform(-16, -18)

    bpy.data.objects["wall_west"].rotation_euler[2] = math.radians(random.uniform(-5, 5))
    bpy.data.objects["wall_east"].rotation_euler[2] = bpy.data.objects["wall_west"].rotation_euler[2]

    bpy.data.objects["camera"].rotation_euler[2] = math.radians(90 + random.uniform(-10, 10))

    bpy.data.objects["wardrobe"].rotation_euler[2] = math.radians(2.95187 + random.uniform(-5, 5))

    bpy.data.objects["light_1"].location.y = random.uniform(-14.3, -9.3)
    bpy.data.objects["light_2"].location.y = random.uniform(-14.3, -9.3)
    bpy.data.objects["light_3"].location.y = random.uniform(-14.3, -9.3)

    bpy.data.objects["wardrobe"].location.x = random.uniform(16, 8)
    x_chaisetable = random.uniform(17, 8)
    bpy.data.objects["chaise1"].location.x = x_chaisetable - 2.5
    bpy.data.objects["chaise2"].location.x = x_chaisetable + 1
    bpy.data.objects["table"].location.x = x_chaisetable

    # Half the time inverse table and wardrobe
    if random.uniform(0, 1) > 0.5:
        temp = bpy.data.objects["table"].location.y
        bpy.data.objects["table"].location.y = bpy.data.objects["wardrobe"].location.y
        bpy.data.objects["chaise1"].location.y = bpy.data.objects["wardrobe"].location.y
        bpy.data.objects["chaise2"].location.y = bpy.data.objects["wardrobe"].location.y
        bpy.data.objects["wardrobe"].location.y = temp
        bpy.data.objects["wardrobe"].rotation_euler[2] += math.radians(180)

    # Sometime, no meuble
    if random.uniform(0.0, 1.0) > 0.9:
        bpy.data.objects["wardrobe"].location.x = 99

    if random.uniform(0.0, 1.0) > 0.9:
        bpy.data.objects["chaise1"].location.x = 99

    if random.uniform(0.0, 1.0) > 0.9:
        bpy.data.objects["chaise2"].location.x = 99

    if random.uniform(0.0, 1.0) > 0.9:
        bpy.data.objects["table"].location.x = 99

    line_v = bpy.data.objects["line"].data.splines[0].bezier_points

    for i in reversed(range(len(line_v))):
        line_v[i].co.y = bpy.data.objects["camera"].location.y + random.uniform(-0.6, 0.6)

        if i == len(line_v) - 2 :
            line_v[i].co.y += (-12 - bpy.data.objects["camera"].location.y)
        if i != len(line_v) - 1 :
            line_v[i].co.y = line_v[i + 1].co.y + random.uniform(-0.3, 0.3)
        line_v[i].handle_left = line_v[i].co
        line_v[i].handle_left.x -= 1
        line_v[i].handle_right = line_v[i].co
        line_v[i].handle_right.x += 1

    bpy.data.objects["circle_1"].location = line_v[len(line_v) - 2].co
    bpy.data.objects["circle_1"].location.z += 0.035

    bpy.data.objects["circle_2"].location = line_v[len(line_v) - 3].co
    bpy.data.objects["circle_2"].location.z += 0.035

    bpy.context.scene.render.filepath = "/home/leandro/Desktop/HEIG/IAA/labo2/render_label/"+str(iteration)+".png"
    bpy.context.scene.render.image_settings.file_format = 'PNG'
    bpy.context.scene.render.image_settings.color_mode = 'RGB'
    bpy.ops.render.render(write_still=True)

    bpy.data.objects["circle_1"].location.z = -1
    bpy.data.objects["circle_2"].location.z = -1

    bpy.context.scene.render.filepath = "/home/leandro/Desktop/HEIG/IAA/labo2/render/"+str(iteration)+".png"
    bpy.context.scene.render.image_settings.file_format = 'PNG'
    bpy.context.scene.render.image_settings.color_mode = 'BW'
    bpy.ops.render.render(write_still=True)
