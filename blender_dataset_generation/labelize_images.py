# This scripts parses images in render_label and create a json file with all the labels.
# It scans the image to find where there is the most red and green pixels, which are the "from" and "to" point respectively.

import cv2
import numpy as np
import json

def find_blob_centers(image_path):
    # Load the image
    image = cv2.imread(image_path)

    # Convert the image to HSV color space
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    # Define the lower and upper bounds for green and red in HSV
    lower_green = np.array([40, 40, 40])
    upper_green = np.array([80, 255, 255])

    lower_red1 = np.array([0, 70, 50])
    upper_red1 = np.array([10, 255, 255])
    lower_red2 = np.array([170, 70, 50])
    upper_red2 = np.array([180, 255, 255])

    # Threshold the HSV image to get only green and red colors
    mask_green = cv2.inRange(hsv, lower_green, upper_green)
    mask_red1 = cv2.inRange(hsv, lower_red1, upper_red1)
    mask_red2 = cv2.inRange(hsv, lower_red2, upper_red2)
    mask_red = cv2.bitwise_or(mask_red1, mask_red2)

    # Find contours in the binary images
    contours_green, _ = cv2.findContours(mask_green, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours_red, _ = cv2.findContours(mask_red, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Get the moments of the contours to calculate the centers
    green_center = None
    red_center = None

    if contours_green:
        green_moment = cv2.moments(max(contours_green, key=cv2.contourArea))
        green_center = (int(green_moment["m10"] / green_moment["m00"]), int(green_moment["m01"] / green_moment["m00"]))

    if contours_red:
        red_moment = cv2.moments(max(contours_red, key=cv2.contourArea))
        if red_moment["m00"] == 0 or red_moment["m01"] == 0: 
            print("ERREUR " + str(iteration))
        red_center = (int(red_moment["m10"] / red_moment["m00"]), int(red_moment["m01"] / red_moment["m00"]))

    return {"from": red_center, "to": green_center}

json_data = "[\n"

for iteration in range(500):
    image_path = "render_label/"+str(iteration)+".png"
    centers = find_blob_centers(image_path)
    json_data += "  " + json.dumps(centers) + ",\n"

json_data = json_data[:-2]
json_data += "\n]\n"

with open("labels.json", 'w') as file:
    file.write(json_data)

