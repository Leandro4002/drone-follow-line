import torch
import torch.nn as nn

class PointFinder(nn.Module):
    def __init__(self):
        super(PointFinder, self).__init__()
        self.conv = nn.Conv2d(in_channels=1, out_channels=4, kernel_size=3, stride=1, padding=1)
        self.fc = nn.Linear(200 * 200 * 4, 4)  # Adjust input size based on image resolution
    
    def forward(self, x):
        x = nn.functional.relu(self.conv(x))
        x = torch.flatten(x, 1)  # Flatten the output of conv layer
        x = self.fc(x)
        x = torch.sigmoid(x) * 200  # Map the values from 0 to 200
        return x