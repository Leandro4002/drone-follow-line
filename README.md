Date: 2024-04-15

Authors:
* Leandro SARAIVA MAIA
* Oscar BAUME

# Description

This project trains a deep learning model that predicts where is located a line given a black and white image.

The dataset used is the **LEANDRONE_V1** image collection [https://huggingface.co/datasets/Leandro4002/LEANDRONE_V1](https://huggingface.co/datasets/Leandro4002/LEANDRONE_V1)

# Install

* Make sure you have ffmpeg installed with `sudo apt install ffmpeg`
* Install the required python package with `pip install -r requirements.txt`
* Open `generate_model.ipynb` and run all the cell in order. This will create the file `followLineModel.pth`.

# Dataset

The dataset has been generated in blender with 3d assets, there are 500 images. Some furniture is placed in a 3d scene and randomness is applied so the generated images have some variations. The white line is also random. The dataset has images with a resolution of 320x320 and are labelled with 2 points named "from" and "to" that draw a line in the direction forward.
We choose to create our the dataset with a 3d program because we can get a lot of images more easily than taking them in real life. We also tried to mimick the angle and context of the camera of the drone.

The images were resized to 200x200 and the labels had to be changed accordingly. There is also an edge detection filter applied to the image so it is easier for the model to predict the outcome. 80% of images are used for training, 20% for validation and they are randomized when loaded.

<img src="image_transformation.png" width="450"/>

# Model architecture

The architecture uses a layer of convolution with a 3x3 kernel and a linear layer that gets down the dimensions to 4 values.

```
PointFinder(
  (conv): Conv2d(1, 4, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (fc): Linear(in_features=160000, out_features=4, bias=True)
)
```

It is enough to make the inference work. We don't want it to be complicated because since it is embedded, it should not be heavy on resources. By the way, maybe the edge detection filter adds too much latency already, it's difficult to be sure without testing.

The training takes 5-10 minutes and we found that training more than 300 epochs gives negligible improvements.

To evaluate the model accuracy, we computed the distance in pixels at which the point is from it's target.
We also use the "loss" value of pytorch.

Overall the model can give a rough estimate of the direction in which the drone must turn so it's acceptable. However, there is still room for improvement.

<img src="example_predictions.png" width="600"/>
